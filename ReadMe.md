# PowerPing

A basic script. It takes an object contianing hostnames and pings them. You can ping as many as you like in one go. Not much more to say really.

## Example

```PowerShell
Import-Module ".\src\PowerPing.psm1" -Force

$Hostnames = Get-Content -Path ".\src\TestHosts.txt"
$Results = Get-PingResult -Hostnames $Hostnames

$Results | Select-Object -Property Address, ProtocolAddress, ReplyInconsistency, StatusCode
```
