
Function Get-PingResult {

    param (
        [Parameter(Mandatory=$true)] [Object] $Hostnames,
        [Parameter(Mandatory=$false)] [int] $ThrottleLimit = 5
    )

    [Object] $Jobs = @()

    foreach ($Host in $Hostnames) {

        $Jobs += Test-Connection -ComputerName $Host -Count 1 -TimeToLive 60 -AsJob -ThrottleLimit $ThrottleLimit

    }

    Wait-Job -Job $Jobs | Out-Null

    $Results = Receive-Job -Job $Jobs

    return $Results | Select-Object -Property Address, ProtocolAddress, ReplyInconsistency, StatusCode

}
