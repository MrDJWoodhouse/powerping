Clear-Host

Import-Module ".\src\PowerPing.psm1" -Force

$Hostnames = Get-Content -Path ".\src\TestHosts.txt"
$Results = Get-PingResult -Hostnames $Hostnames

$Results | Select-Object -Property Address, ProtocolAddress, ReplyInconsistency, StatusCode
